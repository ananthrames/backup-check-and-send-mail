#Import libraries

import pipes
import os
import time
import datetime
import smtplib

#Setting global variable

dtime=time.strftime('%Y-%m-%d')

#Checking backup directories with du command

def DuBackup():
        mail_subject=open("/home/user/mail/mail.txt", "w")
        mail_subject.write("Subject:Daily Backup Status \nHi  \n\nPlease find daily backup status below\n\n----\n\n")
        mail_subject.close()
        du_command="du " " -sch " " /mnt/backup/bkp/*/daily/*" " |" " grep " + pipes.quote(dtime) + " >>" "/home/user/mail/mail.txt"
        du="du " " -sch"
        du_appserver_command="ssh " " user@192.168.1.2 " " 'du -sch'" " /data/App1_dbbackup/daily/* " " | " " grep " + pipes.quote(dtime) + " >>" "/home/user/mail/mail.txt"
        os.system(du_appserver_command)
        os.system(du_command)

#Sending mail using sendmail

def MailSend():
        sendmail_command="sendmail " "admin@example.com " "< " "/home/user/mail/mail.txt "
        os.system(sendmail_command)
DuBackup()
MailSend()